# Generate state diagram from initial collision vector
### Usage :
```
$ python colvec.py <initial_collision_vector>
```
The output will be in dot format, so you might as well pipe it to `dot`, so the 
final command might look like
```
$ python colvec.py <initial_collision_vector> | dot -Tx11:cairo
```
There is also a `-t` switch, which, given an additional argument N, will make 
the program loop through all possible ICVs of length N, i.e. N bit binary 
strings, and find out the maximum number of states in them. (Spoiler alert: 
it's going to be string with a single 1 at MSB and all 0s).
```
$ python colvec.py -t 10
```
