import sys
import itertools

def generate_states(vec, size):
    remaining = [vec]
    states = []
    uniqstates = set([vec])
    for a in remaining:
        #print(remaining)
        #input()
        temp = bin(a)[2:]
        ssize = len(temp)
        for pos, x in enumerate(temp):
            if x == "0":
                result = a >> (ssize - pos)
                # perform the bitwise OR
                result |= a
                #print(result)
                states.append((a, ssize - pos, result))
                if result not in uniqstates:
                    uniqstates.add(result)
                    remaining.append(result)
        if ssize < size:
            states.extend([(a, i, a) for i in range(ssize + 1, size + 1)])
    return (states, remaining)

def stringify(s, size):
    return (bin(s)[2:]).zfill(size)

def dot_print(states, size):
    print("digraph states {")
    for state in states:
        print("\"%s\" -> \"%s\" [label=\"%s\"];"
              % (stringify(state[0], size), stringify(state[2], size), str(state[1])))
    print("}")

def generate_vectors(n):
    return [i for i in range(2**n)]

def eval_single(vectors, size):
    maxstate, maxvector = 0, None
    for v in vectors:
        states, size = generate_states(v, size)
        size = len(size)
        if size > maxstate:
            maxstate = size
            maxvector = v
            #print("Vector : %s\tStates : %d" % (v, size))
    return (maxvector, maxstate)

def eval_parallel(vectors, size):
    from multiprocessing import Pool, cpu_count
    from operator import itemgetter
    workers = cpu_count()
    total = len(vectors)
    part = int(total/workers)
    rem = total - (part * (workers - 1))
    parts = [part] * workers
    parts[-1] = rem
    vector_parts = []
    start = 0
    for p in parts:
        vector_parts.append(vectors[start:(start+p)])
        start += p
    del vectors
    pool = Pool(processes=workers)
    sizes = [size] * workers
    args = zip(vector_parts, sizes)
    ret = pool.starmap(eval_single, args)
    pool.close()
    maxvector, maxstate = max(ret, key=itemgetter(1))
    return (maxvector, maxstate)


def evalute(vectors, size):
    import time
    res = None
    try:
        #raise ImportError
        start = time.perf_counter()
        res = eval_parallel(vectors, size)
    except ImportError:
        start = time.perf_counter()
        res = eval_single(vectors, size)
    print("Maximum number of states %d was in vector %s! (took %3.10fs)"
          % (res[1], stringify(res[0], size), time.perf_counter() - start))

def main():
    autogen = False
    if len(sys.argv) > 2:
        print("// Ignoring additional arguments")
    vector = ""
    if len(sys.argv) == 1:
        vector = input("Enter the collsion vector : ")
    else:
        if len(sys.argv) == 3 and sys.argv[1] == "-t":
            autogen = True
            vector = generate_vectors(int(sys.argv[2]))
        else:
            vector = sys.argv[1]
    binary = ["0", "1"]
    # validate the vector
    if not autogen:
        for i in vector:
            if i not in binary:
                print("// Invalid element in collsion vector : '%s'!" % i)
                sys.exit(1)
    if autogen:
        evalute(vector, int(sys.argv[2]))
    else:
        size = len(vector)
        vector = int(''.join(vector), 2)
        arcs, states = generate_states(vector, size)
        dot_print(arcs, size)
        print("// Number of arcs : ", len(arcs))
        print("// Number of states : ", len(states))
        print("// States : ", [stringify(s, size) for s in states])
if __name__ == "__main__":
    main()
